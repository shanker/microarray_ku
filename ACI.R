#This script performs microarray data analysis, provides list of up- and down-regulated signals and plots 2-way cluster heatmap using log transformed raw signals
#Representative reference: Quackenbush . nature genetics . volume 32 . december 2002
#Data source: http://www.ebi.ac.uk/arrayexpress/experiments/E-GEOD-4083/
getwd()                                                             #current directory
setwd(file.path("Documents","Glycomics","JGB","microarray_ku"))      #setting up working directory
input <- read.table("ACI-ctl-vs-trt.txt", sep="\t", header=T)
#select relevant columns
input <- input[, c('CompositeElementREF',
               'ACI_ctrl_1_VALUE', 'ACI_ctrl_1_DETECTION_P_VALUE',
               'ACI_ctrl_2_VALUE', 'ACI_ctrl_2_DETECTION_P_VALUE',
               'ACI_ctrl_3_VALUE', 'ACI_ctrl_3_DETECTION_P_VALUE',
               'ACI_trt_1_VALUE', 'ACI_trt_1_DETECTION_P_VALUE',
               'ACI_trt_2_VALUE', 'ACI_trt_2_DETECTION_P_VALUE',
               'ACI_trt_3_VALUE', 'ACI_trt_3_DETECTION_P_VALUE',
               'ACI_trt_4_VALUE', 'ACI_trt_4_DETECTION_P_VALUE')]
#Total intensity normalizing factor
  input$SumCtrl <- (input$ACI_ctrl_1_VALUE+input$ACI_ctrl_2_VALUE+input$ACI_ctrl_3_VALUE)                    #sum of control samples
  input$SumTrt <- (input$ACI_trt_1_VALUE+input$ACI_trt_2_VALUE+input$ACI_trt_3_VALUE+input$ACI_trt_4_VALUE)           #sum of treated samples
  N <- sum(input$SumTrt)/sum(input$SumCtrl)               #total intensity normalizing factor
#Sample mean
  input$MeanCtrlSig <- (input$ACI_ctrl_1_VALUE+input$ACI_ctrl_2_VALUE+input$ACI_ctrl_3_VALUE)/3              #mean of control samples
  input$MeanCtrlpVal <- (input$ACI_ctrl_1_DETECTION_P_VALUE+input$ACI_ctrl_2_DETECTION_P_VALUE+input$ACI_ctrl_3_DETECTION_P_VALUE)/3             #mean of control sample p-values
  input$MeanTrtSig <- (input$ACI_trt_1_VALUE+input$ACI_trt_2_VALUE+input$ACI_trt_3_VALUE+input$ACI_trt_4_VALUE)/4     #mean of treated samples
  input$MeanTrtpVal <- (input$ACI_trt_1_DETECTION_P_VALUE+input$ACI_trt_2_DETECTION_P_VALUE+input$ACI_trt_3_DETECTION_P_VALUE+input$ACI_trt_4_DETECTION_P_VALUE)/4    #mean of treated sample p-values
#Data distribution visualization
  hist(input$MeanCtrlSig, main='Control ACI Samples', xlab='Signal')
  hist(input$MeanTrtSig, main='Treatmen ACI Samples', xlab='Signal')
  #Log-transformation
    input$LogMeanCtrlSig <- log(input$MeanCtrlSig)
    input$LogMeanTrtSig <- log(input$MeanTrtSig)
    hist(input$LogMeanCtrlSig, breaks=20, main='Control ACI Samples', xlab='Signal')
    hist(input$LogMeanTrtSig, breaks=20, main='Treatment ACI Samples', xlab='Signal')
#MA plot
  #Log of ratio and product
    input$Ratio=input$MeanTrtSig/input$MeanCtrlSig
    input$Logratio=log2(input$Ratio)
    input$Prod_int=input$MeanTrtSig*input$MeanCtrlSig
    input$LogProdInt=log10(input$Prod_int)
  #Normalization
    #Total intensity normalization
    input$TotIntNorm=input$MeanTrtSig/(N*input$MeanCtrlSig)
    input$LogTotIntNorm=log2(input$TotIntNorm)
    #Lowess normalization
    intercept <- summary(lm(input$Logratio ~ input$LogProdInt))$coefficients[1,1]
    coefficient <- summary(lm(input$Logratio ~ input$LogProdInt))$coefficients[2,1]
    input$L=2**(intercept + coefficient * input$LogProdInt)
    input$LowessNorm=input$TotIntNorm/input$L
    input$LogLowessNorm=log2(input$LowessNorm)
  #Plot all points
    plot(input$LogProdInt, input$LogLowessNorm, pch=3, cex=0.3, main="MA plot", xlab="Trt x Control", ylab="Trt / Control")
  #MA plot by Z-statistic
    Mean <- mean(input$LogLowessNorm)
    StdDev <- sd(input$LogLowessNorm)
    input$Z <- (input$LogLowessNorm-Mean)/StdDev
    input$pValue <- 2*pnorm(-abs(input$Z))
    hist(input$Z, breaks=100, main='Z-score distribution', xlab='Signal')
    color1 <- ifelse(input$Z>=1.96,"red", ifelse(input$Z<=-1.96,"green", "black"))
    plot(input$LogProdInt, input$LogLowessNorm, pch=3, cex=0.3, col=color1, 
         main="MA plot by Z-statistic", sub="Z-statistic threshold = ±1.96",
         xlab="Trt x Control", ylab="Trt / Control")
  #FDR or adjusted p-value to address multiple-testing problem
    upregulated <- input[input$Z>=1.96, ]                   #upregulated signals
    downregulated <- input[input$Z<=-1.96, ]                #downregulated signals
    diffcount <- nrow(upregulated)+nrow(downregulated)  #count of differentially regulated signals
    input$iCtrl <- rank(input$MeanCtrlpVal)                 #ranking of control p-values
    input$CtrlFDR <- input$iCtrl*input$MeanCtrlpVal/diffcount #FDR for control p-values
    input$iTrt <- rank(input$MeanTrtpVal)                   #ranking of treatment p-values  
    input$TrtFDR <- input$iTrt*input$MeanTrtpVal/diffcount    #FDR for treatment p-values
  #MA plot by Z-statistic and FDR
    color2 <- ifelse(input$Z>=1.96 & input$CtrlFDR<0.05 & input$TrtFDR<0.05,"red", ifelse(input$Z<=-1.96 & input$CtrlFDR<0.05 & input$TrtFDR<0.05,"green", "black"))
    plot(input$LogProdInt, input$LogLowessNorm, pch=3, cex=0.3, col=color2, 
         main="MA plot by Z-statistic and p-value", sub="Z-statistic threshold = ±2",
         xlab="Trt x Control", ylab="Trt / Control")
#Filter differentially expressed probes
  DiffExp <- input[abs(input$Z)>=1.96 & input$CtrlFDR<0.05 & input$TrtFDR<0.05, ]
  write.table(DiffExp, file="DiffExp.txt", sep="\t", row.names=F)

#Cluster analysis
#Select relevant columns
raw <- DiffExp[, c('CompositeElementREF',
                       'ACI_ctrl_1_VALUE',
                       'ACI_ctrl_2_VALUE',
                       'ACI_ctrl_3_VALUE',
                       'ACI_trt_1_VALUE',
                       'ACI_trt_2_VALUE',
                       'ACI_trt_3_VALUE',
                       'ACI_trt_4_VALUE')]
raw$Ctrl1 <- log(raw$ACI_ctrl_1_VALUE)
raw$Ctrl2 <- log(raw$ACI_ctrl_2_VALUE)
raw$Ctrl3 <- log(raw$ACI_ctrl_3_VALUE)
raw$Trt1 <- log(raw$ACI_trt_1_VALUE)
raw$Trt2 <- log(raw$ACI_trt_2_VALUE)
raw$Trt3 <- log(raw$ACI_trt_3_VALUE)
raw$Trt4 <- log(raw$ACI_trt_4_VALUE)
library('gplots')
row.names(raw) <- raw$CompositeElementREF
d <- data.matrix(raw[,9:15])
distance_r <- dist(d)                                           #distance matrix
hclust_complete_r <- hclust(distance_r, method = "complete")    #hierarchical clustering for miRNAs
dendrow <- as.dendrogram(hclust_complete_r)                     #dendrogram for miRNAs
distance_c <- dist(t(d))                                        #distance matrix
hclust_complete_c <- hclust(distance_c, method = "complete")    #hierarchical clustering for samples
dendcol <- as.dendrogram(hclust_complete_c)                     #dendrogram of samples
pdf(file = "2wayCluster.pdf")
heatmap.2(d, Rowv=dendrow, Colv=dendcol, scale="column", col=colorRampPalette(c("green","black","red"))(256), key=TRUE, lmat=rbind(c(0, 3, 4), c(2, 1, 0)), lhei=c(0.2, 0.95), lwid=c(1, 5, 2), trace='none', density.info=c('none'), cexCol=1.5, cexRow=0.1, margins=c(4, 1))
dev.off()
